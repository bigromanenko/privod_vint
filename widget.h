 #ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtCharts/QAbstractAxis>
#include <ui_widget.h>
using namespace QtCharts;
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
private:
signals:
private slots:
    void on_radioButton_clicked(bool checked);
    void on_radioButton_2_clicked(bool checked);


    void on_checkBox_stateChanged(int arg1);

    void on_pushButton_clicked();

    void on_radioButton_6_clicked(bool checked);

    void on_radioButton_5_clicked(bool checked);


    void on_radioButton_4_clicked(bool checked);

    void on_radioButton_3_clicked(bool checked);

    void on_pushButton_2_clicked();

    void on_comboBox_activated(int index);

    void on_comboBox_2_activated(int index);

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
