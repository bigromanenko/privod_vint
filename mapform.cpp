#include "mapform.h"


MapForm::MapForm(QWidget *parent) :
    QWidget(parent) {
    setupUi(this);

    LineSeries = new QLineSeries;
    scatterSeries = new QScatterSeries;
    scatterSeries->clear();
    LineSeries->clear();
    chart = new QChart;
    chart->addSeries(LineSeries);
    chart->addSeries(scatterSeries);
    xAxis = new QValueAxis;
    yAxis = new QValueAxis;
    xAxis->setRange(0, 100);
    xAxis->setTickCount(11);
    xAxis->setTitleText("Мп, Н*м");
    yAxis->setRange(0, 100);
    yAxis->setTickCount(11);
    yAxis->setTitleText("n, об/мин");
    chartView = new QChartView (chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->chart()->setAxisX(xAxis, LineSeries);
    chartView->chart()->setAxisY(yAxis, LineSeries);
    chartView->chart()->setAxisX(xAxis, scatterSeries);
    chartView->chart()->setAxisY(yAxis, scatterSeries);


    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    setLayout(hlay);

}

MapForm::~MapForm() {

}

void MapForm::Clear(void)
{
     LineSeries->clear();
     scatterSeries->clear();
}
void MapForm::AddLine(double X, double Y)
{
    LineSeries->append(X, 0);
    LineSeries->append(0, Y);
    xAxis->setRange(0, X+2);
    yAxis->setRange(0, Y+250);
}
void MapForm::AddPoint(double X, double Y)
{

    scatterSeries->append(X, Y);

}
