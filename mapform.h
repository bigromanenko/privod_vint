#ifndef MAPFORM_H
#define MAPFORM_H

#include <QWidget>
#include "ui_mapform.h"
#include <QtCharts>
#include <QHBoxLayout>

using namespace QtCharts;
class MapForm : public QWidget, public Ui::MapForm {
    Q_OBJECT
private:
    QChartView * chartView;
    QLineSeries * LineSeries;
    QScatterSeries * scatterSeries;
    QChart * chart;
    QValueAxis * xAxis;
    QValueAxis * yAxis;
    QHBoxLayout * hlay;

public:
    explicit MapForm(QWidget *parent = 0);
    ~MapForm();
    void AddPoint(double X, double Y);
    void AddLine (double X, double Y);
    void Clear(void);
public slots:

private:

};

#endif // MAPFORM_H
