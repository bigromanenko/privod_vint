#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QMap>

using namespace std;
//setlocale(LC_ALL,"Russian");
float p;
float t;
float H;//=0.25;
float D;//=0.25;
float Y=0;
float Cx;
float Sx;
float V;
float nxx;
float Mmax;
float a;
float b;
float Kk;
float K2;
float n;
float Vx;
float VxLast;
float P;
float kpd_dvizh;
float kpd_dvig;
float kpd_poln;
float Dmin;
float Dmax;
float H_Dmin;
float H_Dmax;
float deltaH_D;
float deltaD;
float nmin;
float nmax;
float Z=0;
float Zz=0;
float m;
float H_D;
float Mogr;
char c;
float VxMax=0;
float H_D_VxMax;
float D_VxMax;
float n_VxMax;
float Moment;
float M_shv;
float n_shv;
float P_shv;
float U;
float R0;
float Ce;
float Cm;
float Kr;
float Km;
float Ke;
float KH;
float kpdLast=0;
float kpdMax=0;
float H_D_kpdMax=0;
float D_kpdMax=0;
float n_kpdMax=0;
int flag_mode_calculation=0;
int flag_vint=0;
int flag2;


float Kk1;
float Kk2;

float K21;
float K22;
float K23;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->stackedWidget->hide();
    this->setStyleSheet("background-color: white");
    ui->pushButton->setStyleSheet("background-color: lightgray");
    ui->pushButton_2->setStyleSheet("background-color: lightgray");
}


float func_Z(float a,float D, float Y, float Cx, float Sx, float Kk, float t, float m)
{
    float calc_Z=((-a)*(pow(D,3))*(1-Y)+pow(((pow(a,2))*(pow(D,6))*(pow((1-Y),2))+((2*Cx*Sx*(pow(D,4))*Kk)/(m*(1-t)))),0.5))/((Cx*Sx)/(m*(1-t)));
    return calc_Z;
}
float func_Kk(float H_D, float Kk1, float Kk2)
{
   float calc_Kk=Kk1*(H_D)+(-1)*Kk2;
   return calc_Kk;
}

float func_K2 (float H_D, float K21, float K22, float K23)
{
   float calc_K2=K21*(H_D)*(H_D)+(-1)*K22*(H_D)+(-1)*K23;
    return calc_K2;
}
/*float func_Kk(float H_D)
{
   float calc_Kk=0.7321*(H_D)-0.2493;
   return calc_Kk;
}

float func_K2 (float H_D)
{
   float calc_K2=0.0514*(H_D)*(H_D)-0.0088*(H_D)-0.0002;
    return calc_K2;
}
*/
float calculation_n(float b, int p,float nxx,float Mmax,float D, float Y,float K2, float Z)
{
    float  calc_n=(-1+pow((1+4*(((nxx)/Mmax)*K2*p*(pow(D,5))-((nxx)/Mmax)*b*p*(pow(D,4))*(1-Y)*Z)*(nxx)),0.5))/(2*(((nxx)/Mmax)*K2*p*(pow(D,5))-((nxx)/Mmax)*b*p*(pow(D,4))*(1-Y)*Z));
    return  calc_n;
}
float calculation_Vx (float n, float Z)
{
    float calc_v=n*Z;
    return calc_v;
}
int Repeat_Vmax()
{
    D=Dmin;
    H_D=H_Dmin;
   while(H_D<=H_Dmax)
   {
   while(D<=Dmax)
   {
       VxLast=VxMax;
       Kk=func_Kk(H_D,Kk1,Kk2);
       K2=func_K2(H_D,K21,K22,K23);
       if(((pow(a,2))*(pow(D,6))*(pow((1-Y),2))+((2*Cx*Sx*(pow(D,4))*Kk)/(m*(1-t))))>0)
       {
       Z=func_Z(a,D,Y,Cx,Sx,Kk,t,m);
       if((1+4*(((nxx)/Mmax)*K2*p*(pow(D,5))-((nxx)/Mmax)*b*p*(pow(D,4))*(1-Y)*Z)*(nxx))>0)
       {
          if((2*(((nxx)/Mmax)*K2*p*(pow(D,5))-((nxx)/Mmax)*b*p*(pow(D,4))*(1-Y)*Z)>0))
          {
             if(Z>0)
                {
       n=calculation_n(b,p,nxx,Mmax,D,Y,K2,Z);
       Vx=calculation_Vx(n,Z);
       Moment=p*(pow(n,2))*(pow(D,5))*(K2-b*((Vx*(1-Y))/(n*D)));
       P=p*(pow(n,2))*(pow(D,4))*(Kk-a*((Vx*(1-Y))/(n*D)));
       kpd_dvizh=(P*Vx*(1-Y))/(Moment*n*6.28);
       kpd_dvig=(Moment*n*6.28)/((Moment/(KH*Km*Cm))*1.732*U);
       kpd_poln=kpd_dvizh*kpd_dvig;
       n_shv=(-1+pow((4*((nxx*nxx)/Mmax)*K2*p*pow(D,5)+1),0.5))/(2*(nxx/Mmax)*K2*p*pow(D,5));
       P_shv=Kk*p*pow(n_shv,2)*pow(D,4);
       if(0<n&&n<nxx)
       {
          // if(kpd_dvizh<0.52&&P_shv>P)
           //{
               if(K2>(b*((Vx*(1-Y))/(n*D)))&&(p*(pow(n,2))*(pow(D,5))*(K2-b*((Vx*(1-Y))/(n*D))))<Mogr&&(p*(pow(n,2))*(pow(D,5))*(K2-b*((Vx*(1-Y))/(n*D))))>0)
               {
                  if(P>((Cx*Sx*(p/2)*pow(Vx,2))/(m*(1-t)))-0.5&&P<((Cx*Sx*(p/2)*pow(Vx,2))/(m*(1-t)))+0.5&&Kk>a*((Vx*(1-Y))/(n*D)))
         // if(P==((Cx*Sx*(p/2)*pow(Vx,2))/(m*(1-t)))&&P>0)
                    {
                     if(Vx>VxLast)
                     {
                         VxMax=Vx;
                         H_D_VxMax=H_D;
                         D_VxMax=D;
                         n_VxMax=n;
                     }
                    }
               }
          }
       }
       }
       }
       }
       D=D+deltaD;
   }
   H_D=H_D+deltaH_D;
   D=Dmin;
   }
   Kk=func_Kk(H_D_VxMax,Kk1,Kk2);
   K2=func_K2(H_D_VxMax,K21,K22,K23);
   P=p*(pow(n_VxMax,2))*(pow(D_VxMax,4))*(Kk-a*((VxMax*(1-Y))/(n_VxMax*D_VxMax)));
   Moment=p*(pow(n_VxMax,2))*(pow(D_VxMax,5))*(K2-b*((VxMax*(1-Y))/(n_VxMax*D_VxMax)));
   kpd_dvizh=(P*VxMax*(1-Y))/(Moment*n_VxMax*6.28);
   kpd_dvig=(Moment*n_VxMax*6.28)/((Moment/(KH*Km*Cm))*1.732*U);
   kpd_poln=kpd_dvizh*kpd_dvig;
   n_shv=(-1+pow((4*((nxx*nxx)/Mmax)*K2*p*pow(D_VxMax,5)+1),0.5))/(2*(nxx/Mmax)*K2*p*pow(D_VxMax,5));
   M_shv=K2*p*pow(n_shv,2)*pow(D_VxMax,5);
   P_shv=Kk*p*pow(n_shv,2)*pow(D_VxMax,4);
   return 0;
}


int Repeat_KPDmax()
{
    D=Dmin;
    H_D=H_Dmin;
   while(H_D<=H_Dmax)
   {
   while(D<=Dmax)
   {
     //Widget::ui->lineEdit_28->setText(QString::number(H_D));
       //ui->lineEdit_29->setText(QString::number(D));
       kpdLast=kpdMax;
       Kk=func_Kk(H_D,Kk1,Kk2);
       K2=func_K2(H_D,K21,K22,K23);
       Z=func_Z(a,D,Y,Cx,Sx,Kk,t,m);
       float A1=(Cx*Sx*pow(Vx,2))/(2*m*(1-t));
       float A2=(nxx/Mmax)*b*Vx*(1-Y)*p;
       float Discr=pow((1-A2*pow(D,4)-a*pow(D,3)*Vx*(1-Y)),2)+4*(Kk*pow(D,4)+(nxx/Mmax)*p*pow(D,5)*K2)*(nxx+A1);
       n=((-(1-A2*pow(D,4)-a*pow(D,3)*Vx*(1-Y)))+pow(Discr,0.5))/(2*(Kk*pow(D,4)+(nxx/Mmax)*p*pow(D,5)*K2));

       Moment=p*(pow(n,2))*(pow(D,5))*(K2-b*((Vx*(1-Y))/(n*D)));
       P=p*(pow(n,2))*(pow(D,4))*(Kk-a*((Vx*(1-Y))/(n*D)));
       kpd_dvizh=(P*Vx*(1-Y))/(Moment*n*6.28);
       kpd_dvig=(Moment*n*6.28)/((Moment/(KH*Km*Cm))*1.732*U);
       kpd_poln=kpd_dvizh*kpd_dvig;
       //n_shv=(-1+pow((4*((nxx*nxx)/Mmax)*K2*p*pow(D,5)+1),0.5))/(2*(nxx/Mmax)*K2*p*pow(D,5));
      // P_shv=Kk*p*pow(n_shv,2)*pow(D,4);
       if(0<n&&n<nxx)
         {
          if(Moment>0&&Moment<=Mogr&&kpd_dvizh<0.9)
             {
              if(P>((Cx*Sx*(p/2)*pow(Vx,2))/(m*(1-t)))-0.5&&P<((Cx*Sx*(p/2)*pow(Vx,2))/(m*(1-t)))+0.5&&P>0)
                {
                if(kpd_poln>kpdLast)
                  {
                   kpdMax=kpd_poln;
                   H_D_kpdMax=H_D;
                   D_kpdMax=D;
                   n_kpdMax=n;
                  }
                }
             }
         }
    D=D+deltaD;
   }
   H_D=H_D+deltaH_D;
   D=Dmin;
   }
   Kk=func_Kk(H_D_kpdMax,Kk1,Kk2);
   K2=func_K2(H_D_kpdMax,K21,K22,K23);
   P=p*(pow(n_kpdMax,2))*(pow(D_kpdMax,4))*(Kk-a*((Vx*(1-Y))/(n_kpdMax*D_kpdMax)));
   Moment=p*(pow(n_kpdMax,2))*(pow(D_kpdMax,5))*(K2-b*((Vx*(1-Y))/(n_kpdMax*D_kpdMax)));
   kpd_dvizh=(P*Vx*(1-Y))/(Moment*n_kpdMax*6.28);
   kpd_dvig=(Moment*n_kpdMax*6.28)/((Moment/(KH*Km*Cm))*1.732*U);
   kpd_poln=kpd_dvizh*kpd_dvig;

   n_shv=(-1+pow((4*((nxx*nxx)/Mmax)*K2*p*pow(D_kpdMax,5)+1),0.5))/(2*(nxx/Mmax)*K2*p*pow(D_kpdMax,5));
   M_shv=K2*p*pow(n_shv,2)*pow(D_kpdMax,5);
   P_shv=Kk*p*pow(n_shv,2)*pow(D_kpdMax,4);
   return 0;

}
Widget::~Widget()
{
    delete ui;
}

void Widget::on_radioButton_clicked(bool checked)
{
    if (checked)
    {
    ui->stackedWidget->show();
    ui->stackedWidget->setCurrentWidget(ui->page);
    }


}

void Widget::on_radioButton_2_clicked(bool checked)
{
    if (checked)
    {
    ui->stackedWidget->show();
    ui->stackedWidget->setCurrentWidget(ui->page_2);
    ui->checkBox->setCheckState(Qt::Unchecked);
    }
}

void Widget::on_checkBox_stateChanged(int arg1)
{
    if (arg1)
    {
   // ui->stackedWidget->show();
   // ui->stackedWidget->setCurrentWidget(ui->page_2);
    emit on_radioButton_2_clicked(true);
        ui->radioButton_2->setChecked(true);
    }
}

void Widget::on_pushButton_clicked()
{

    p=ui->lineEdit->text().toInt();
    t=ui->lineEdit_2->text().toFloat();
    Y=ui->lineEdit_3->text().toFloat();
    Cx=ui->lineEdit_4->text().toFloat();
    Sx=ui->lineEdit_5->text().toFloat();
    H_Dmin=ui->lineEdit_6->text().toFloat();
    H_Dmax=ui->lineEdit_7->text().toFloat();
    deltaH_D=ui->lineEdit_8->text().toFloat();
    Dmin=ui->lineEdit_9->text().toFloat();
    Dmax=ui->lineEdit_10->text().toFloat();
    deltaD=ui->lineEdit_11->text().toFloat();
    m=ui->lineEdit_23->text().toFloat();
  //  nxx=ui->lineEdit_21->text().toFloat();
  //  nxx=nxx/60;
  //  Mmax=ui->lineEdit_20->text().toFloat();
    Mogr=ui->lineEdit_22->text().toFloat();
    U=ui->lineEdit_12->text().toFloat();
    R0=ui->lineEdit_13->text().toFloat();
    Ce=ui->lineEdit_14->text().toFloat();
    Cm=ui->lineEdit_15->text().toFloat();
    Kr=ui->lineEdit_16->text().toFloat();
    Km=ui->lineEdit_17->text().toFloat();
    Ke=ui->lineEdit_18->text().toFloat();
    KH=ui->lineEdit_19->text().toFloat();

    Mmax=KH*Km*Cm*(U/(Kr*R0));
    nxx=((2*KH*U)/(Ke*Ce*6.28));

    if(flag_vint==1)
    {
        a=0.556;
        b=0.028;


        Kk1=0.7321;
        Kk2=0.2493;

        K21=0.0514;
        K22=0.0088;
        K23=0.0002;
    }

    if(flag_vint==2)
    {
        a=0.432;
        b=0.0618;

        Kk1=0.431;
        Kk2=-0.007;

        K21=0.089;
        K22=0.035;
        K23=-0.012;
    }

    if (flag_mode_calculation==1)
    {
    Repeat_Vmax();
    ui->lineEdit_24->setText(QString::number(VxMax));
    ui->lineEdit_26->setText(QString::number(60*n_VxMax));
    ui->lineEdit_27->setText(QString::number(Moment));
    ui->lineEdit_28->setText(QString::number(H_D_VxMax));
    ui->lineEdit_29->setText(QString::number(D_VxMax));

    ui->lineEdit_25->setText(QString::number(p*(pow(n_VxMax,2))*(pow(D_VxMax,4))*(Kk-a*((VxMax*(1-Y))/(n_VxMax*D_VxMax)))));
    ui->lineEdit_30->setText(QString::number(kpd_dvizh));
    ui->lineEdit_32->setText(QString::number(kpd_dvig));
    ui->lineEdit_35->setText(QString::number(kpd_poln));
    ui->lineEdit_33->setText(QString::number(n_shv*60));
    ui->lineEdit_34->setText(QString::number(M_shv));
    ui->lineEdit_37->setText(QString::number(P_shv));
    ui->lineEdit_38->setText(QString::number((Moment/(KH*Km*Cm))*1.732*U));
    ui->widget_1->AddLine(Mmax,nxx*60);
    ui->widget_1->AddPoint(Moment,n_VxMax*60);
    }

    if (flag_mode_calculation==2)
    {
    Vx=ui->lineEdit_31->text().toFloat();
    Repeat_KPDmax();
    ui->lineEdit_24->setText(QString::number(Vx));
    ui->lineEdit_26->setText(QString::number(60*n_kpdMax));
    ui->lineEdit_27->setText(QString::number(Moment));
    ui->lineEdit_28->setText(QString::number(H_D_kpdMax));
    ui->lineEdit_29->setText(QString::number(D_kpdMax));
    ui->lineEdit_25->setText(QString::number(p*(pow(n_kpdMax,2))*(pow(D_kpdMax,4))*(Kk-a*((Vx*(1-Y))/(n_kpdMax*D_kpdMax)))));
    ui->lineEdit_30->setText(QString::number(kpd_dvizh));
    ui->lineEdit_32->setText(QString::number(kpd_dvig));
    ui->lineEdit_35->setText(QString::number(kpd_poln));
    ui->lineEdit_33->setText(QString::number(n_shv*60));
    ui->lineEdit_34->setText(QString::number(M_shv));
    ui->lineEdit_37->setText(QString::number(P_shv));
    ui->lineEdit_38->setText(QString::number((Moment/(KH*Km*Cm))*1.732*U));
    ui->widget_1->AddLine(Mmax,nxx*60);
    ui->widget_1->AddPoint(Moment,n_kpdMax*60);
    }
    p=0;
    t=0;
    H=0;
    D=0;
    Y=0;
    Cx=0;
    Sx=0;
    V=0;
    nxx=0;
    Mmax=0;
    a=0;
    b=0;
    Kk=0;
    K2=0;
    n=0;
    Vx=0;
    VxLast=0;
    P=0;
    kpd_dvizh=0;
    kpd_dvig=0;
    kpd_poln=0;
    Dmin=0;
    Dmax=0;
    H_Dmin=0;
    H_Dmax=0;
    deltaH_D=0;
    deltaD=0;
    nmin=0;
    nmax=0;
    Z=0;
    Zz=0;
    m=0;
    H_D=0;
    Mogr=0;
    c=0;
    VxMax=0;
    H_D_VxMax=0;
    D_VxMax=0;
    n_VxMax=0;
    Moment=0;
    M_shv=0;
    n_shv=0;
    P_shv=0;
    U=0;
    R0=0;
    Ce=0;
    Cm=0;
    Kr=0;
    Km=0;
    Ke=0;
    KH=0;
    kpdLast=0;
    kpdMax=0;
    H_D_kpdMax=0;
    D_kpdMax=0;
    n_kpdMax=0;



    Kk1=0;
    Kk2=0;

    K21=0;
    K22=0;
    K23=0;
}

void Widget::on_radioButton_6_clicked(bool checked)
{
    if (checked)
    {
    flag_mode_calculation=2;
    ui->stackedWidget_2->show();
    ui->stackedWidget_2->setCurrentWidget(ui->page_4);

    }
}

void Widget::on_radioButton_5_clicked(bool checked)
{
    if (checked)
    {
    flag_mode_calculation=1;
    ui->stackedWidget_2->close();
    ui->stackedWidget_2->setCurrentWidget(ui->page_5);

    }
}
struct engine_parametr
{
    float Uss;
    float R00;
    float Cee;
    float Cmm;
};
/*
QMap<QString,param> engine;
engine.insert_iterator("5ДБМ70-1.5-4-3","5ДБМ70-1.5-4-3".param);
*/
void Widget::on_radioButton_4_clicked(bool checked)
{
    if(checked)
    {
    flag_vint=1;
    }
}

void Widget::on_radioButton_3_clicked(bool checked)
{
    if(checked)
    {
    flag_vint=2;
    }

}

void Widget::on_pushButton_2_clicked()
{
      ui->widget_1->Clear();
}

void Widget::on_comboBox_activated(int index)
{
    if(index==0)
    {
         U=300;
         R0=5.5;
         Ce=0.46;
         Cm=0.46;
    }
    if(index==6)
    {
         U=27;
         R0=0.485;
         Ce=0.12;
         Cm=0.12;
    }
    ui->lineEdit_12->setText(QString::number(U));
    ui->lineEdit_13->setText(QString::number(R0));
    ui->lineEdit_14->setText(QString::number(Ce));
    ui->lineEdit_15->setText(QString::number(Cm));

}


void Widget::on_comboBox_2_activated(int index)
{
    if(index==0)
    {
        Kr=1;
        Km=1.5;
        Ke=2;
        KH=1;
    }
    if(index==1)
    {
        Kr=1.5;
        Km=1.5;
        Ke=2;
        KH=1;
    }
    if(index==3)
    {
        Kr=1;
        Km=1;
        Ke=1.5;
        KH=1;
    }

    ui->lineEdit_16->setText(QString::number(Kr));
    ui->lineEdit_17->setText(QString::number(Km));
    ui->lineEdit_18->setText(QString::number(Ke));
    ui->lineEdit_19->setText(QString::number(KH));
}

